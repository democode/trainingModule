angular.module('mod.idn')
//ALL SERVICE CALLS
    .factory('ResetService', ['ResetFactory', '$q', 'SessionService', function(ResetFactory, $q, SessionService){

        var factory = {};

        factory.resetPassword = function(resetObject){
            var deferred = $q.defer();

            ResetFactory.resetPassword().reset(resetObject, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return factory;
    }])


    //ALL FACTORY DEFINITIONS START HERE

    .factory('ResetFactory', ['$resource', 'MOD_IDN', 'AppService', 'SessionService', function($resource, MOD_IDN, AppService, SessionService){
        var factory = {};

        factory.resetPassword = function(){

            var bearer = "Bearer " + AppService.token.access_token;
            return $resource(MOD_IDN.API_URL + 'auth/forgotPassword', {},{
                'reset' :{
                    method : 'POST',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            });
        };

        return factory;

    }]);