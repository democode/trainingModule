'use strict';

angular.module('mod.idn')
    .controller('BugViewCtrl', ['$scope', function($scope){

        $scope.totalIssues = 42;
        $scope.bugView =
            {
                bugId : 55,
                title : "This bug is causing problems",
                priority : "4",
                status : "Open",
                description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!",
                dateTime : "08 July 2016",
                user : {
                    name : "Steve Rogers",
                    email : "cap@avengers.com"
                },
                comments : [
                    {
                    bugId : 55,
                    title : "This bug is causing problems",
                    priority : "4",
                    status : "Open",
                    description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!",
                    dateTime : "08 July 2016",
                    user : {
                        name : "Steve Rogers",
                        email : "cap@avengers.com"
                    },
                }]
            };

        $scope.currentNavItem = 0;

        $scope.goto = function(pageType){
            console.log("Page changed");
        };


        var init = function(){
            $scope.currentNavItem = 'openIssuesPage';
        };

        init();
    }]);