'use strict';

angular.module('mod.idn')
.controller('BugReportingCtrl', ['$scope', function($scope){

    $scope.totalIssues = 42;
    $scope.allReports = [
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
        {
            title : "This bug is causing problems",
            priority : "4",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet commodi culpa dicta doloremque ducimus earum esse fugit illo, in inventore minima optio, porro, rem repudiandae sapiente suscipit tenetur ut!"
        },
    ];

    $scope.currentNavItem = 0;

    $scope.goto = function(pageType){
        console.log("Page changed");
    };


    var init = function(){
        $scope.currentNavItem = 'openIssuesPage';
    };

    init();
}]);