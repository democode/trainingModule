/**
 * Created by nbos on 3/14/16.
 */
angular.module('mod.idn')
    .controller('ForgotCtrl', ['$scope', 'ForgotService', 'AlertService', 'SessionService', '$state', 'UserService', 'FBService', 'GoogleService', function($scope, ForgotService, AlertService, SessionService, $state, UserService, FBService, GoogleService) {

        $scope.user_email='';

        $scope.message = '';
        
        $scope.forgotPassword = function(){
            console.log("in")
            ForgotService.sendEmail($scope.user_email).then(function(success){
                console.log(success);
                $scope.message = "Please check your email for reset instructions."
            }, function(error){
                $scope.message = "Oops! The email address specified does not exist in our records."
                console.log(error);
            });
        };









    }]);
