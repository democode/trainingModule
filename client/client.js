'use strict';

angular.module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'underscore',
        

        'app.config',
        'app.layout',
        'app.theme',
        'mod.nbos',
        'mod.idn',
        'mod.m43'
])

angular.module('clientApp')
.constant('CLIENT_CONFIG',{
        CLIENT_ID: '5225667a-0e45-4c35-ab9c-6dc4c48d978b',
        CLIENT_SECRET: 'modulesecret',
        CLIENT_DOMAIN : 'http://localhost:9001'
})
