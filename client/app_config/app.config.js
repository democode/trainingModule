'use strict';

angular.module('app.config', []);


angular.module('app.config')

    .constant('APP_CONSTANTS', {
        API_URL : 'http://api.qa1.nbos.in/',
        // API_URL : 'http://10.9.8.53:8080/',
        API_URL_DEV : 'http://api.qa1.nbos.in/',
        TENANT_ID: 'TNT:appConsole',
        CLIENT_ID:'appConsole-app-client',
        CLIENT_SECRET:'appConsole-app-secret',
        GRANT_TYPE:'client_credentials',
        SCOPE:'',
        APP_SESSION_KEY:'TNT:appConsole'
    })

    .value('APP_CONFIG', {})

    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home',{
                url:'/home',
                templateUrl:"app_config/firstPage/firstPage.html",
                controller:'FirstPageCtrl',
                data: {
                    type: 'login',
                    authenticate : false
                }
            })
    }]);
