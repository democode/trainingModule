angular.module('mod.m43', []);

angular.module('mod.m43')
  .constant('MOD_trainingModule', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m43', {
                url: '/trainingModule',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m43.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_trainingModule/views/menu/trainingModule.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "trainingModule"
               }
           })
            .state('m43.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_trainingModule/views/dashboard/trainingModule.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "trainingModule"
                }

            })
            .state('m43.reminders', {
                url: '/reminders',
                templateUrl: "mod_trainingModule/views/remindersPage/reminders.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Reminders",
                    module: "trainingModule"
                }

            })
            .state('m43.reminderView', {
                url: '/reminders/:reminderId',
                templateUrl: "mod_trainingModule/views/remindersPage/reminderView.html",
                data: {
                    type: 'home',
                    menu: false,
                    name: "Reminder View",
                    module: "trainingModule"
                }

            })

            //FOR APP ADMIN
           .state('m43.admin', {
               url: '/admin',
               templateUrl: "mod_trainingModule/views/admin/trainingModule.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "trainingModule"
               }
           })

    }])