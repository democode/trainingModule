'use strict';

angular.module('mod.m43')
.controller('ReminderViewCtrl', ['$scope', '$stateParams', '$state', function($scope, $stateParams, $state){

    
    $scope.reminderId = $stateParams.reminderId;

    
    var init = function(){
        if($scope.reminderId){
            //continue
        } else {
            //no reminder id, go back to reminders page
            $state.go('m43.reminders');
        }
    };

    init();
}]);