'use strict';

angular.module('mod.m43')
.controller('RemindersCtrl', ['$scope', '$state', function($scope, $state){

    $scope.message = "Hello from you module Dashboard";



    var list = [
    {
    	id: 1,
    	title : "My first Reminder",
    	description : "asdf asdf asdf asdf asdf "
    },
    {
    	id: 2,
    	title : "My second Reminder",
    	description : "asdf asdf asdf asdf asdf "
    },
    {
    	id: 3,
    	title : "My third Reminder",
    	description : "asdf asdf asdf asdf asdf "
    }
    ];

    var getAllMyReminder = function(){
    	//call the server
    	//get the dat
    	$scope.remidersList = list;
    };

    $scope.showInfo = function(id){
    	$state.go('m43.reminderView', {reminderId : id});
    };


    
    var init = function(){
        getAllMyReminder();
    };

    init();
}]);