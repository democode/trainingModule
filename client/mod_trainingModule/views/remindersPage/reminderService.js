'use strict';

angular.module('mod.nbos')

.service('ManageService', ['ManageFactory', '$q', function(ManageFactory, $q) {
    var factory = {};

    factory.project;

    factory.projectAbout;

    factory.getProject = function(tenantId) {

        var deferred = $q.defer();
        ManageFactory.project().get({ tenantId: tenantId }, function(success) {
            factory.project = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    factory.getAbout = function(tenantId) {

        var deferred = $q.defer();
        ManageFactory.projectAbout().get({ tenantId: tenantId }, function(success) {
            console.log(success);
            factory.projectAbout = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    factory.saveAbout = function(about) {
        var deferred = $q.defer();
        ManageFactory.projectAbout().save(about, function(success) {
            factory.projectAbout = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };


    return factory;

}])

.factory('ManageFactory', ['MOD_NBOS', 'SessionService', '$resource', 'Upload', '$q', function(MOD_NBOS, SessionService, $resource, Upload, $q) {
    var factory = {};

    factory.project = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'tenant/v0/tenants/:tenantId/show', {}, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };


    factory.projectAbout = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'about/v0/tenants/:tenantId/about', { tenantId: '@tenantId' }, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                }
            },
            'save': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    return factory;
}])
